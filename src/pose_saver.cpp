#include "ros/ros.h"
#include <ros/console.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <tf2_msgs/TFMessage.h>
#include <tf/transform_listener.h>
#include <sys/types.h>

#include <time.h>
#include <sstream>
#include <stdlib.h>

using namespace std;

class pose_keep{

public:

    pose_keep(){
		//ROS_INFO_STREAM( "tf: " );
		point_name="table0";
		save_flag=false;

		std::string save_location_folder_path;
		ros::param::get( "save_location_folder_path", save_location_folder_path );

		if( save_location_folder_path == "" )
		{
			std::cout << "save_location_folder_path is empty." << std::endl;
			while(true){ros::Duration(10).sleep();}
		}

		time_t now = time(NULL);
		struct tm *pnow = localtime(&now);

	  	file_name << save_location_folder_path << "/saved_location_"<< pnow->tm_mon + 1 <<"_"<< pnow->tm_mday <<"_"<< pnow->tm_hour <<"_"<< pnow->tm_min <<".yaml";

		while(true)
		{
			//tfで変換
			tf::StampedTransform transform;
			listener.waitForTransform("/map","base_footprint",ros::Time(0),ros::Duration(3.0));
			listener.lookupTransform("/map","base_footprint",ros::Time(0),transform);

			/*
			ROS_INFO_STREAM( "transform.getOrigine().x(): "<<transform.getOrigin().x() );
			ROS_INFO_STREAM( "transform.getOrigine().y(): "<<transform.getOrigin().y() );
			ROS_INFO_STREAM( "transform.getOrigine().z(): "<<transform.getOrigin().z() );
			ROS_INFO_STREAM( "transform.getRotation().x(): "<<transform.getRotation().x() );
			ROS_INFO_STREAM( "transform.getRotation().y(): "<<transform.getRotation().y() );
			ROS_INFO_STREAM( "transform.getRotation().z(): "<<transform.getRotation().z() );
			ROS_INFO_STREAM( "transform.getRotation().w(): "<<transform.getRotation().w() );
			*/

			if (save_flag==true)
		    {
				save_flag=false;
		        //保存する．
				point_name_vec.push_back(point_name);
				translation_x. push_back(transform.getOrigin().x() );
				translation_y. push_back(transform.getOrigin().y() );
				translation_z. push_back(transform.getOrigin().z() );

				rotation_x. push_back(transform.getRotation().x() );
				rotation_y. push_back(transform.getRotation().y() );
				rotation_z. push_back(transform.getRotation().z() );
				rotation_w. push_back(transform.getRotation().w() );

				ROS_INFO_STREAM( "transform.getOrigine().x(): "<<transform.getOrigin().x() );
				ROS_INFO_STREAM( "transform.getOrigine().y(): "<<transform.getOrigin().y() );
				ROS_INFO_STREAM( "transform.getOrigine().z(): "<<transform.getOrigin().z() );

				ROS_INFO_STREAM( "transform.getRotation().x(): "<<transform.getRotation().x() );
				ROS_INFO_STREAM( "transform.getRotation().y(): "<<transform.getRotation().y() );
				ROS_INFO_STREAM( "transform.getRotation().z(): "<<transform.getRotation().z() );
				ROS_INFO_STREAM( "transform.getRotation().w(): "<<transform.getRotation().w() );

				ROS_INFO_STREAM(  "I'v kept it.\n");
		   	}//if



			if(save_flag==false)
			{
				ROS_INFO_STREAM(  "前回("<< point_name <<")とは違うキーを入力してください。「q」で終了。" );
				while(true)
				{
					cin >> point_name;
					if (point_name=="\n" )
					{
						ROS_INFO_STREAM(  point_name  );
					}//if
					else
					{
						if(point_name =="q")
						{
							ROS_INFO_STREAM(  "OK,I'll end...."  );


							ofstream ofs(file_name.str().c_str());
							if(ofs)
							{
							for(int i=0;i<point_name_vec.size();i++)
								{
									ofs << std::endl;
									ofs << "# "<< point_name_vec[i] << " #" << std::endl;
									ofs << std::endl;
									ofs << "/" << point_name_vec[i] << "_translation_x: " << translation_x[i] << std::endl;
									ofs << "/" << point_name_vec[i] << "_translation_y: " << translation_y[i] << std::endl;
									ofs << "/" << point_name_vec[i] << "_translation_z: " << translation_z[i] << std::endl;
									ofs << std::endl;
									ofs << "/" << point_name_vec[i] << "_rotation_x: " << rotation_x[i] << std::endl;
									ofs << "/" << point_name_vec[i] << "_rotation_y: " << rotation_y[i] << std::endl;
									ofs << "/" << point_name_vec[i] << "_rotation_z: " << rotation_z[i] << std::endl;
									ofs << "/" << point_name_vec[i] << "_rotation_w: " << rotation_w[i] << std::endl;
									ofs << "#======================================#" << std::endl;
								}//for
								ofs.close();
								ROS_INFO_STREAM(  "「　" << file_name.str() << "　」として保存完了。"  );
							}//if
							else
							{
								ofs.close();
								ROS_INFO_STREAM(  file_name.str() << "　は作成できませんでした。ｍ（_ _;）ｍ"  );
							
							}//else						
							ROS_INFO_STREAM( "終了→　xボタンで閉じて" );							
							while(1){ros::Duration(10).sleep();}
							exit(EXIT_SUCCESS);
						}//if
						save_flag=true;
						break;
					}//else
				}//while
			}//if
		}//while




	}//pose_keep()
	~pose_keep(){}

	

private:
 	ros::NodeHandle nh;
    ros::Subscriber sub_posi;

	string point_name;
	bool save_flag;

	stringstream file_name;

	vector<string> point_name_vec;

	vector<float> translation_x;
	vector<float> translation_y;
	vector<float> translation_z;

	vector<float> rotation_x;
	vector<float> rotation_y;
	vector<float> rotation_z;
	vector<float> rotation_w;

	tf::TransformListener listener;
};//pose_keep




int main(int argc, char **argv)
{
	ros::init(argc, argv, "pose_saver_edu");
	ROS_INFO_STREAM( "\nキーを入力すると座標を保存" );
	pose_keep pk;//上の関数を実行
	ros::spin();
	return 0;
}
